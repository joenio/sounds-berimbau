# sounds-berimbau

`sounds-berimbau` is a curated pack of sample files copied from [open, free,
libre][libre] projects containing sounds of [Berimbau][berimbau], a popular musical
instrument in Brazil, commonly used during the practice of the art martial
[Capoeira][capoeira].

The plan is to include this samples pack into [Clean-Samples][clean-samples]
project.

## License

GPLv3

## Copyright

Joenio Marques da Costa <joenio@joenio.me>

See the copyright and license for each sample in the file
[copyright](/copyright).

[libre]: https://www.gnu.org/philosophy/free-software-even-more-important.html
[berimbau]: https://en.wikipedia.org/wiki/Berimbau
[capoeira]: https://en.wikipedia.org/wiki/Capoeira
[clean-samples]: https://github.com/tidalcycles/Clean-Samples
